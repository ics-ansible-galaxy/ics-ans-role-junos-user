ics-ans-role-junos-user
===================

Ansible role to add, update or remove user accounts on Junos devices.
- conda create --name junos --file requirements.txt
- conda activate junos
- 
Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
#See defaults/main.yml for a working example.
ics_ans_role_junos_user:
ics_ans_role_junos_key:
ics_ans_role_junos_port:
ics_junos_list_users:
ics_junos_base_url: "https://host/path/.."
  1:
    name: <account>
    # can be 'read-only', 'super-user', 'operator' or 'unauthorized' 
    role: <role>
    #path to sshkey
    sshkey: <sshkey>
    # can be 'present' or 'absent'
    state: <sate>
  2:
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-junos-user
```

License
-------

BSD 2-clause
